# bin_packing

A solution to the bin packing problem in Common Lisp.

Some requirements:

* Manage different size of container, in this case vans.
* We have one size of crate measured by cubic meters
* We have a list of vegetables and fruits categorized by their size
* An enumerated list of requests to order crates by requests
* And we need to order crates by weight


# References

http://www.cs.ucsb.edu/~teo/cs130b.w15/bp.pdf

http://www.golems.org/papers/SchusterIROS10-palletizing.pdf

# Author

+ Dan Loaiza <papachan@gmail.com>

# Copyright

Copyright (c) 2018 Dan Loaiza <papachan@gmail.com>

# License

Licensed under the BSD License.
