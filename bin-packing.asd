;;;; bin-packing.asd

(asdf:defsystem #:bin-packing
  :author "Dan Loaiza <papachan@gmail.com>"
  :license  "BSD"
  :depends-on (:fiveam
               :bin-packing-test)
  :serial t
  :components (
               (:module "src"
                :serial t
                :components
                ((:file "package")
                 (:file "main"))))
  :description "bin packing problem"
  :long-description #.(uiop:read-file-string
                       (uiop:subpathname *load-pathname* "README.md"))
  :perform (test-op
            (o s)
            (uiop:symbol-call :fiveam '#:run!
                              (uiop:find-symbol* '#:all-tests
                                                 :bin-packing-test))))
