;;; -*- Mode: Lisp; Syntax: Common-Lisp -*-
;;;; main.lisp

(in-package #:bin-packing)

(defun hello ()
  (format t "~A" '(1 2)))

(hello)
