;;; -*- Mode: Lisp; Syntax: Common-Lisp -*-
;;; package.lisp
(asdf:oos 'asdf:load-op :fiveam)

(defpackage bin-packing-test
  (:use :cl
        :fiveam
        :bin-packing))
